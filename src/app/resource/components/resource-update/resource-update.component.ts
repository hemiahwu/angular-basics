import { Component, Input } from '@angular/core';
import { Resource, ResourceAlert } from '../../shared/resource.model';
import { Observable } from 'rxjs'

@Component({
  selector: 'app-resource-form',
  templateUrl: './resource-update.component.html',
  styleUrls: ['./resource-update.component.scss']
})
export class ResourceUpdateComponent {
  @Input() onSubmit: (resource: Resource) => Observable<Resource>
  @Input() alert: ResourceAlert;;

  selectedResource: Resource;
  types = Resource.types;

  @Input() set resource(selectedResource: Resource) {
    this.selectedResource = { ...selectedResource };
  }
  submitForm() {
    this.onSubmit(this.selectedResource);
  }

}
