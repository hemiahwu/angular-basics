
import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { Resource } from './resource.model'
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  constructor(private http: HttpClient) {

  }

  private handleError(errorResponse: HttpErrorResponse): Observable<never> {
    let message;

    if (errorResponse.error instanceof ErrorEvent) {
      message = errorResponse.error.message;
    } else {
      message = errorResponse.error;
    }

    return throwError(message);
  }

  // Observabe === Promise  异步请求
  getResources(): Observable<Resource[]> {
    return this.http.get<Resource[]>("/api/resources")
  }

  getResourceById(id: string): Observable<Resource> {
    return this.http.get<Resource>(`/api/resources/${id}`)
  }

  searchResources(searchedTerm: string): Observable<Resource[]> {
    return this.http.get<Resource[]>(`/api/resources/s/${searchedTerm}`)
  }

  createResource(body: Resource): Observable<Resource> {
    return this.http.post<Resource>(`/api/resources`, body)
      .pipe(catchError(this.handleError))
  }

  updateResource(id: string, body: Resource): Observable<Resource> {
    return this.http.patch<Resource>(`/api/resources/${id}`, body)
      .pipe(catchError(this.handleError))
  }

  deleteResource(id: string): Observable<Resource> {
    return this.http.delete<Resource>(`/api/resources/${id}`)
  }
}