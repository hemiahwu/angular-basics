export class ResourceAlert {
    success: string;
    error: string;
}

export class Resource {
    static readonly types = ["book", "blog", "video"]

    _id: string;
    title: string;
    description: string;
    type: string;
    link: string;

    constructor({ title = '', description = '', type = '', link = '' } = {}) {
        this.title = title;
        this.description = description;
        this.type = type;
        this.link = link;
    }
}