import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertComponent } from './shared/alert.component';

import { Resource } from './shared/resource.model';
import { ResourceService } from './shared/resource.service'

@Component({
    selector: 'app-resource',
    templateUrl: './resource.component.html'
})

export class ResourceComponent extends AlertComponent implements OnInit, OnDestroy {
    public isDetailView = true;
    public selectedResource: Resource;
    public resources = []

    constructor(private resourceService: ResourceService) {
        super();
    }

    // 初始化函数
    ngOnInit() {
        // 发起请求
        this.getResources();
    }

    ngOnDestroy(): void {
        // alert("将要离开当前页面,销毁组件")
        this.clearAlertTimeout();
    }

    private getResources() {
        // http请求
        this.resourceService.getResources()
            .subscribe((resources: Resource[]) => {
                // console.log(resources);
                this.resources = resources;
            })
    }

    get resourcesCount(): number {
        return this.resources.length
    }

    toggleView() {
        this.isDetailView = !this.isDetailView;
    }

    get btnViewClass(): string {
        return this.isDetailView ? "btn-warning" : "btn-primary";
    }

    private seleteResource(resource: Resource): Resource {
        if (!resource || !resource._id) {
            this.selectedResource = null;
            return null;
        }
        this.selectedResource = { ...resource };
        return this.selectedResource;
    }

    public handleResourceSelect(resource: Resource) {
        this.clearAlertTimeout();
        this.seleteResource(resource);
    }

    get hasResources(): boolean {
        return this.resources && this.resourcesCount > 0;
    }

    get activeResource(): Resource {
        return (this.selectedResource || (this.hasResources && this.resources[0]) || null)
    }

    private findResourceIndex(resource: Resource): number {
        return this.resources.findIndex(r => r._id === resource._id);
    }

    public hydrateResources(resource: Resource) {
        const index = this.findResourceIndex(resource);
        this.resources[index] = resource;
        this.seleteResource(resource);
    }

    public deleteResource() {
        const isConfirm = confirm("Are you sure you want to delete this");
        if (!this.activeResource?._id) {
            alert("No Resource Selected :)");
            return;
        }

        if (isConfirm) {
            // 发起删除请求
            this.resourceService.deleteResource(this.activeResource._id).subscribe(dResource => {
                const index = this.findResourceIndex(dResource);
                this.resources.splice(index, 1)
                this.seleteResource(this.resources[0])
            })
        }
    }
    public updateResource = (resource: Resource) => {
        this.resourceService.updateResource(resource._id, resource)
            .subscribe((updateResource) => {
                this.hydrateResources(updateResource);
                // 弹窗方法
                this.setAlert('success', "Resource was updated")
            }, (error: string) => {
                this.setAlert("error", error)
            })
    }

    public handleSearch(searchedTitle: string) {
        // console.log(searchedTitle)
        if (!searchedTitle) {
            return this.getResources();
        }

        this.resourceService.searchResources(searchedTitle)
            .subscribe(resources => {
                this.resources = resources;
                this.seleteResource(null)
                !this.isDetailView ? this.isDetailView = true : null;
            })
    }

}