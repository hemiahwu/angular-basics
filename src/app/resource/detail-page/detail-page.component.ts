import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Resource } from '../shared/resource.model';
import { ResourceService } from '../shared/resource.service';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  resource: Resource

  constructor(private route: ActivatedRoute, private resourceService: ResourceService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id']
      this.getResource(id)
    })
  }

  private getResource(id: string) {
    this.resourceService.getResourceById(id)
      .subscribe(resource => {
        this.resource = resource
      })
  }

}
