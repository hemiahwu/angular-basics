import { Component, OnInit } from '@angular/core';
import { AlertComponent } from '../shared/alert.component';
import { Resource } from '../shared/resource.model';
import { ResourceService } from '../shared/resource.service';

@Component({
  selector: 'app-resource-new',
  templateUrl: './resource-new.component.html',
  styleUrls: ['./resource-new.component.scss']
})
export class ResourceNewComponent extends AlertComponent {

  resource = new Resource();

  constructor(private resourceService: ResourceService) {
    super();
  }

  public createResource = (resource: Resource) => {
    this.resourceService.createResource(resource)
      .subscribe((newResource) => {
        // 弹窗方法
        this.setAlert('success', "Resource was created")
      }, (error: string) => {
        this.setAlert("error", error)
      })
  }

}
